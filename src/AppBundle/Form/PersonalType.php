<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PersonalType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('entidad', EntityType::class, array(
                    'class' => 'AppBundle:Entidad',
                    'query_builder' => function (\AppBundle\Repository\EntidadRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'nombre',
                    "label" => "Entidad",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('nombres', TextType::class, array("label" => "Nombres",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('apellidos', TextType::class, array("label" => "Apellidos",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('tipoDocumento', ChoiceType::class, array('choices' => array(
                        'C.C' => "C.C",
                        'C.E' => "C.E",
                    ), "label" => "Tipo de Documento",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('documento', TextType::class, array("label" => "Documento",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('fechaIngreso', BirthdayType::class, array("label" => "Fecha Ingreso",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('estadoCivil', ChoiceType::class, array('choices' => array(
                        'Soltero(a)' => "Soltero(a)",
                        'Casado(a)' => "Casado(a)",
                        'Viudo(a)' => "Viudo(a)",
                        'Separado(a)' => "Separado(a)",
                    ), "label" => "Estado Civil",
                    "required" => false,
                    "attr" => array('class' => 'form-control')))
                ->add('direccion', TextareaType::class, array("label" => "Direccion de Residencia",
                    "required" => false,
                    "attr" => array('class' => 'form-control')))
                ->add('ciudad', TextType::class, array("label" => "Ciudad",
                    "required" => false,
                    "attr" => array('class' => 'form-control')))
                ->add('nivel', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 1')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Nivel",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('codigo', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 2')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Codigo",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('grado', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 3')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Grado",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('descripcion', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 4')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Descripcion",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('centroCosto', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 5')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Centro de Costo",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('regimen', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 6')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Regimen",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('fondoCesantias', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 7')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Fondo de Cesantias",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('fondoPensiones', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 8')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Fondo de Pensiones",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('saludEps', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 9')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Salud EPS",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('genero', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 10')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Genero",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('funcionarios', EntityType::class, array(
                    'class' => 'AppBundle:Registro',
                    'query_builder' => function (\AppBundle\Repository\RegistroRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->where('r.tipoCampo = 11')
                                ->orderBy('r.nombre', 'ASC');
                    },
                    'choice_label' => 'descripcion',
                    "label" => "Funcionarios",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('gastosRepresentacion', TextType::class, array("label" => "Gastos Representacion",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('horasExtra', TextType::class, array("label" => "Horas Extra",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))
                ->add('primaTecnica', TextType::class, array("label" => "Prima Tecnica",
                    "required" => true,
                    "attr" => array('class' => 'form-control')))

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Personal'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_personal';
    }

}
