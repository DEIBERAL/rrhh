<?php

namespace AppBundle\Controller;

use AppBundle\Entity\TipoCampo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Tipocampo controller.
 *
 */
class TipoCampoController extends Controller
{
    /**
     * Lists all tipoCampo entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tipoCampos = $em->getRepository('AppBundle:TipoCampo')->findAll();

        return $this->render('tipocampo/index.html.twig', array(
            'tipoCampos' => $tipoCampos,
        ));
    }

    /**
     * Creates a new tipoCampo entity.
     *
     */
    public function newAction(Request $request)
    {
        $tipoCampo = new Tipocampo();
        $form = $this->createForm('AppBundle\Form\TipoCampoType', $tipoCampo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tipoCampo);
            $em->flush();

            return $this->redirectToRoute('tipocampo_show', array('id' => $tipoCampo->getId()));
        }

        return $this->render('tipocampo/new.html.twig', array(
            'tipoCampo' => $tipoCampo,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a tipoCampo entity.
     *
     */
    public function showAction(TipoCampo $tipoCampo)
    {
        $deleteForm = $this->createDeleteForm($tipoCampo);

        return $this->render('tipocampo/show.html.twig', array(
            'tipoCampo' => $tipoCampo,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing tipoCampo entity.
     *
     */
    public function editAction(Request $request, TipoCampo $tipoCampo)
    {
        $deleteForm = $this->createDeleteForm($tipoCampo);
        $editForm = $this->createForm('AppBundle\Form\TipoCampoType', $tipoCampo);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('tipocampo_edit', array('id' => $tipoCampo->getId()));
        }

        return $this->render('tipocampo/edit.html.twig', array(
            'tipoCampo' => $tipoCampo,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a tipoCampo entity.
     *
     */
    public function deleteAction(Request $request, TipoCampo $tipoCampo)
    {
        $form = $this->createDeleteForm($tipoCampo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tipoCampo);
            $em->flush();
        }

        return $this->redirectToRoute('tipocampo_index');
    }

    /**
     * Creates a form to delete a tipoCampo entity.
     *
     * @param TipoCampo $tipoCampo The tipoCampo entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TipoCampo $tipoCampo)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tipocampo_delete', array('id' => $tipoCampo->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
