<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Personal
 *
 * @ORM\Table(name="PERSONAL", schema="Personal", 
 * indexes={
 * @ORM\Index(name="FK_PERSONAL_ENTIDAD", columns={"ENTIDAD"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_CENTRO_COSTO", columns={"CENTRO_COSTO"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_CODIGO", columns={"CODIGO"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_DESCRIPCION", columns={"DESCRIPCION"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_FONDO_CESANTIAS", columns={"FONDO_CESANTIAS"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_FONDO_PENSIONES", columns={"FONDO_PENSIONES"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_FUNCIONARIOS", columns={"FUNCIONARIOS"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_GENERO", columns={"GENERO"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_GRADO", columns={"GRADO"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_NIVEL", columns={"NIVEL"}),
 * @ORM\Index(name="FK_PERSONAL_REGISTRO_SALUD_EPS", columns={"SALUD_EPS"})
 * }
 * )
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonalRepository")
 */
class Personal {

    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="NOMBRES", type="string", length=255)
     */
    private $nombres;

    /**
     * @var string
     *
     * @ORM\Column(name="APELLIDOS", type="string", length=255)
     */
    private $apellidos;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO_DOCUMENTO", type="string", length=255)
     */
    private $tipoDocumento;

    /**
     * @var int
     *
     * @ORM\Column(name="DOCUMENTO", type="integer")
     */
    private $documento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA_INGRESO", type="datetime")
     */
    private $fechaIngreso;

    /**
     * @var string
     *
     * @ORM\Column(name="ESTADO_CIVIL", type="string", length=255)
     */
    private $estadoCivil;

    /**
     * @var string
     *
     * @ORM\Column(name="DIRECCION", type="string", length=255)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="CIUDAD", type="string", length=255)
     */
    private $ciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="TELEFONO_FIJO", type="string", length=255)
     */
    private $telefonoFijo;

    /**
     * @var string
     *
     * @ORM\Column(name="TELEFONO_CELULAR", type="string", length=255)
     */
    private $telefonoCelular;

    /**
     * @var string
     *
     * @ORM\Column(name="CORREO_ELECTRONICO", type="string", length=255)
     */
    private $correo;

    /**
     * @var string
     *
     * @ORM\Column(name="FOTO_PERFIL", type="string", length=255)
     */
    private $fotoPerfil;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="NIVEL", referencedColumnName="ID")
     * })
     */
    private $nivel;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CODIGO", referencedColumnName="ID")
     * })
     */
    private $codigo;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="GRADO", referencedColumnName="ID")
     * })
     */
    private $grado;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DESCRIPCION", referencedColumnName="ID")
     * })
     */
    private $descripcion;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CENTRO_COSTO", referencedColumnName="ID")
     * })
     */
    private $centroCosto;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="REGIMEN", referencedColumnName="ID")
     * })
     */
    private $regimen;

    /**
     * @var integer
     *
     * @ORM\Column(name="GASTOS_REPRESENTACION", type="integer", length=255)
     */
    private $gastosRepresentacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="PRIMA_TECNICA", type="integer", length=255)
     */
    private $primaTecnica;

    /**
     * @var integer
     *
     * @ORM\Column(name="HORAS_EXTRAS", type="integer", length=255)
     */
    private $horasExtra;

    /**
     * @var integer
     *
     * @ORM\Column(name="RIESGOS_PROFESIONALES", type="integer", length=255)
     */
    private $riesgosProfesionales;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FONDO_CESANTIAS", referencedColumnName="ID")
     * })
     */
    private $fondoCesantias;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FONDO_PENSIONES", referencedColumnName="ID")
     * })
     */
    private $fondoPensiones;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SALUD_EPS", referencedColumnName="ID")
     * })
     */
    private $saludEps;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="GENERO", referencedColumnName="ID")
     * })
     */
    private $genero;

    /**
     * @var \AppBundle\Entity\Registro
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Registro" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FUNCIONARIOS", referencedColumnName="ID")
     * })
     */
    private $funcionarios;

    /**
     * @var \AppBundle\Entity\Entidad
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Entidad" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ENTIDAD", referencedColumnName="ID")
     * })
     */
    private $entidad;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nombres
     *
     * @param string $nombres
     *
     * @return Personal
     */
    public function setNombres($nombres) {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * Get nombres
     *
     * @return string
     */
    public function getNombres() {
        return $this->nombres;
    }

    /**
     * Set apellidos
     *
     * @param string $apellidos
     *
     * @return Personal
     */
    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * Get apellidos
     *
     * @return string
     */
    public function getApellidos() {
        return $this->apellidos;
    }

    /**
     * Set tipoDocumento
     *
     * @param string $tipoDocumento
     *
     * @return Personal
     */
    public function setTipoDocumento($tipoDocumento) {
        $this->tipoDocumento = $tipoDocumento;

        return $this;
    }

    /**
     * Get tipoDocumento
     *
     * @return string
     */
    public function getTipoDocumento() {
        return $this->tipoDocumento;
    }

    /**
     * Set documento
     *
     * @param integer $documento
     *
     * @return Personal
     */
    public function setDocumento($documento) {
        $this->documento = $documento;

        return $this;
    }

    /**
     * Get documento
     *
     * @return int
     */
    public function getDocumento() {
        return $this->documento;
    }

    /**
     * Set fechaIngreso
     *
     * @param \DateTime $fechaIngreso
     *
     * @return Personal
     */
    public function setFechaIngreso($fechaIngreso) {
        $this->fechaIngreso = $fechaIngreso;

        return $this;
    }

    /**
     * Get fechaIngreso
     *
     * @return \DateTime
     */
    public function getFechaIngreso() {
        return $this->fechaIngreso;
    }

    /**
     * Set estadoCivil
     *
     * @param string $estadoCivil
     *
     * @return Personal
     */
    public function setEstadoCivil($estadoCivil) {
        $this->estadoCivil = $estadoCivil;

        return $this;
    }

    /**
     * Get estadoCivil
     *
     * @return string
     */
    public function getEstadoCivil() {
        return $this->estadoCivil;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Personal
     */
    public function setDireccion($direccion) {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion() {
        return $this->direccion;
    }

    /**
     * Set ciudad
     *
     * @param string $ciudad
     *
     * @return Personal
     */
    public function setCiudad($ciudad) {
        $this->ciudad = $ciudad;

        return $this;
    }

    /**
     * Get ciudad
     *
     * @return string
     */
    public function getCiudad() {
        return $this->ciudad;
    }


    /**
     * Set telefonoFijo
     *
     * @param string $telefonoFijo
     *
     * @return Personal
     */
    public function setTelefonoFijo($telefonoFijo)
    {
        $this->telefonoFijo = $telefonoFijo;

        return $this;
    }

    /**
     * Get telefonoFijo
     *
     * @return string
     */
    public function getTelefonoFijo()
    {
        return $this->telefonoFijo;
    }

    /**
     * Set telefonoCelular
     *
     * @param string $telefonoCelular
     *
     * @return Personal
     */
    public function setTelefonoCelular($telefonoCelular)
    {
        $this->telefonoCelular = $telefonoCelular;

        return $this;
    }

    /**
     * Get telefonoCelular
     *
     * @return string
     */
    public function getTelefonoCelular()
    {
        return $this->telefonoCelular;
    }

    /**
     * Set correo
     *
     * @param string $correo
     *
     * @return Personal
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get correo
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set fotoPerfil
     *
     * @param string $fotoPerfil
     *
     * @return Personal
     */
    public function setFotoPerfil($fotoPerfil)
    {
        $this->fotoPerfil = $fotoPerfil;

        return $this;
    }

    /**
     * Get fotoPerfil
     *
     * @return string
     */
    public function getFotoPerfil()
    {
        return $this->fotoPerfil;
    }

    /**
     * Set gastosRepresentacion
     *
     * @param integer $gastosRepresentacion
     *
     * @return Personal
     */
    public function setGastosRepresentacion($gastosRepresentacion)
    {
        $this->gastosRepresentacion = $gastosRepresentacion;

        return $this;
    }

    /**
     * Get gastosRepresentacion
     *
     * @return integer
     */
    public function getGastosRepresentacion()
    {
        return $this->gastosRepresentacion;
    }

    /**
     * Set primaTecnica
     *
     * @param integer $primaTecnica
     *
     * @return Personal
     */
    public function setPrimaTecnica($primaTecnica)
    {
        $this->primaTecnica = $primaTecnica;

        return $this;
    }

    /**
     * Get primaTecnica
     *
     * @return integer
     */
    public function getPrimaTecnica()
    {
        return $this->primaTecnica;
    }

    /**
     * Set horasExtra
     *
     * @param integer $horasExtra
     *
     * @return Personal
     */
    public function setHorasExtra($horasExtra)
    {
        $this->horasExtra = $horasExtra;

        return $this;
    }

    /**
     * Get horasExtra
     *
     * @return integer
     */
    public function getHorasExtra()
    {
        return $this->horasExtra;
    }

    /**
     * Set riesgosProfesionales
     *
     * @param integer $riesgosProfesionales
     *
     * @return Personal
     */
    public function setRiesgosProfesionales($riesgosProfesionales)
    {
        $this->riesgosProfesionales = $riesgosProfesionales;

        return $this;
    }

    /**
     * Get riesgosProfesionales
     *
     * @return integer
     */
    public function getRiesgosProfesionales()
    {
        return $this->riesgosProfesionales;
    }

    /**
     * Set nivel
     *
     * @param \AppBundle\Entity\Registro $nivel
     *
     * @return Personal
     */
    public function setNivel(\AppBundle\Entity\Registro $nivel)
    {
        $this->nivel = $nivel;

        return $this;
    }

    /**
     * Get nivel
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getNivel()
    {
        return $this->nivel;
    }

    /**
     * Set codigo
     *
     * @param \AppBundle\Entity\Registro $codigo
     *
     * @return Personal
     */
    public function setCodigo(\AppBundle\Entity\Registro $codigo)
    {
        $this->codigo = $codigo;

        return $this;
    }

    /**
     * Get codigo
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set grado
     *
     * @param \AppBundle\Entity\Registro $grado
     *
     * @return Personal
     */
    public function setGrado(\AppBundle\Entity\Registro $grado)
    {
        $this->grado = $grado;

        return $this;
    }

    /**
     * Get grado
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getGrado()
    {
        return $this->grado;
    }

    /**
     * Set descripcion
     *
     * @param \AppBundle\Entity\Registro $descripcion
     *
     * @return Personal
     */
    public function setDescripcion(\AppBundle\Entity\Registro $descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set centroCosto
     *
     * @param \AppBundle\Entity\Registro $centroCosto
     *
     * @return Personal
     */
    public function setCentroCosto(\AppBundle\Entity\Registro $centroCosto)
    {
        $this->centroCosto = $centroCosto;

        return $this;
    }

    /**
     * Get centroCosto
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getCentroCosto()
    {
        return $this->centroCosto;
    }

    /**
     * Set regimen
     *
     * @param \AppBundle\Entity\Registro $regimen
     *
     * @return Personal
     */
    public function setRegimen(\AppBundle\Entity\Registro $regimen)
    {
        $this->regimen = $regimen;

        return $this;
    }

    /**
     * Get regimen
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getRegimen()
    {
        return $this->regimen;
    }

    /**
     * Set fondoCesantias
     *
     * @param \AppBundle\Entity\Registro $fondoCesantias
     *
     * @return Personal
     */
    public function setFondoCesantias(\AppBundle\Entity\Registro $fondoCesantias)
    {
        $this->fondoCesantias = $fondoCesantias;

        return $this;
    }

    /**
     * Get fondoCesantias
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getFondoCesantias()
    {
        return $this->fondoCesantias;
    }

    /**
     * Set fondoPensiones
     *
     * @param \AppBundle\Entity\Registro $fondoPensiones
     *
     * @return Personal
     */
    public function setFondoPensiones(\AppBundle\Entity\Registro $fondoPensiones)
    {
        $this->fondoPensiones = $fondoPensiones;

        return $this;
    }

    /**
     * Get fondoPensiones
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getFondoPensiones()
    {
        return $this->fondoPensiones;
    }

    /**
     * Set saludEps
     *
     * @param \AppBundle\Entity\Registro $saludEps
     *
     * @return Personal
     */
    public function setSaludEps(\AppBundle\Entity\Registro $saludEps)
    {
        $this->saludEps = $saludEps;

        return $this;
    }

    /**
     * Get saludEps
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getSaludEps()
    {
        return $this->saludEps;
    }

    /**
     * Set genero
     *
     * @param \AppBundle\Entity\Registro $genero
     *
     * @return Personal
     */
    public function setGenero(\AppBundle\Entity\Registro $genero)
    {
        $this->genero = $genero;

        return $this;
    }

    /**
     * Get genero
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getGenero()
    {
        return $this->genero;
    }

    /**
     * Set funcionarios
     *
     * @param \AppBundle\Entity\Registro $funcionarios
     *
     * @return Personal
     */
    public function setFuncionarios(\AppBundle\Entity\Registro $funcionarios)
    {
        $this->funcionarios = $funcionarios;

        return $this;
    }

    /**
     * Get funcionarios
     *
     * @return \AppBundle\Entity\Registro
     */
    public function getFuncionarios()
    {
        return $this->funcionarios;
    }

    /**
     * Set entidad
     *
     * @param \AppBundle\Entity\Entidad $entidad
     *
     * @return Personal
     */
    public function setEntidad(\AppBundle\Entity\Entidad $entidad)
    {
        $this->entidad = $entidad;

        return $this;
    }

    /**
     * Get entidad
     *
     * @return \AppBundle\Entity\Entidad
     */
    public function getEntidad()
    {
        return $this->entidad;
    }
}
