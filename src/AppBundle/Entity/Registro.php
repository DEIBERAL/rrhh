<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Registro
 *
 * @ORM\Table(name="REGISTRO", schema="Registro",
 * indexes={@ORM\Index(name="FK_REGISTRO_TIPO_CAMPO", columns={"TIPO_CAMPO"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RegistroRepository")
 */
class Registro
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=255)
     */
    private $descripcion;

    /**
     * @var \AppBundle\Entity\TipoCampo
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\TipoCampo" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TIPO_CAMPO_ID", referencedColumnName="ID")
     * })
     */
   
    private $tipoCampo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Registro
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Registro
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     /**
     * Set user
     *
     * @param \AppBundle\Entity\TipoCampo $tipoCampo
     *
     * @return Registro
     */
    public function setTipoCampo(\AppBundle\Entity\TipoCampo $tipoCampo)
    {
        $this->tipoCampo = $tipoCampo;

        return $this;
    }

    /**
     * Get tipoCampo
     *
     * @return \AppBundle\Entity\TipoCampo
     */
    public function getTipoCampo()
    {
        return $this->tipoCampo;
    }
}

