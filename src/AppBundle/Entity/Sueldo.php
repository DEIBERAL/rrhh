<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sueldo
 *
 * @ORM\Table(name="SUELDO", schema="Personal")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SueldoRepository")
 */
class Sueldo
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="valor", type="integer")
     */
    private $valor;

    /**
     * @var int
     *
     * @ORM\Column(name="periodo", type="integer")
     */
    private $periodo;
    /**
     * @var boolean
     *
     * @ORM\Column(name="activo", type="boolean")
     */
    private $activo;

     /**
     * @var \AppBundle\Entity\Personal
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Personal" )
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PERSONAL_ID", referencedColumnName="ID")
     * })
     */
   
    private $tipoCampo;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param integer $valor
     *
     * @return Sueldo
     */
    public function setValor($valor)
    {
        $this->valor = $valor;

        return $this;
    }

    /**
     * Get valor
     *
     * @return int
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set periodo
     *
     * @param integer $periodo
     *
     * @return Sueldo
     */
    public function setPeriodo($periodo)
    {
        $this->periodo = $periodo;

        return $this;
    }

    /**
     * Get periodo
     *
     * @return int
     */
    public function getPeriodo()
    {
        return $this->periodo;
    }

    /**
     * Set activo
     *
     * @param boolean $activo
     *
     * @return Sueldo
     */
    public function setActivo($activo)
    {
        $this->activo = $activo;

        return $this;
    }

    /**
     * Get activo
     *
     * @return boolean
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set tipoCampo
     *
     * @param \AppBundle\Entity\Personal $tipoCampo
     *
     * @return Sueldo
     */
    public function setTipoCampo(\AppBundle\Entity\Personal $tipoCampos)
    {
        $this->tipoCampo = $tipoCampo;

        return $this;
    }

    /**
     * Get tipoCampo
     *
     * @return \AppBundle\Entity\Personal
     */
    public function getTipoCampo()
    {
        return $this->tipoCampo;
    }
}
